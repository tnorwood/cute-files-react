import React, {Component} from 'react';
import './FolderPanel.css';

class FolderPanel extends Component {
  generateAmountString(fileNumber) {
    let amountString = '';

    if(fileNumber === 0) {
      amountString = 'Empty';
    } else if (fileNumber === 1) {
      amountString = `${fileNumber} item`;
    } else {
      amountString = `${fileNumber} items`;
    }

    return amountString;
  }

  render() {
    let fullState = this.props.folder.items.length === 0 ? 'icon folder' : 'icon folder full';
    let amountString = this.generateAmountString(this.props.folder.items.length);

    return (
      <li>
        <div className="link" onClick={this.props.onClick}>
          <span className={fullState}></span>
          <span className="name">{this.props.folder.name}</span>
          <span className="details">{amountString}</span>
        </div>
      </li>
    );
  }
}

export default FolderPanel;
