import React, {Component} from 'react';
import axios from 'axios';
import Display from './Display';
import Breadcrumbs from './Breadcrumbs';
import Searchbar from './Searchbar';
import Elevate from './Elevate';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      files: {name: 'Shared', type: 'folder', path: 'Shared', items: []},
      currentPath: 'Shared',
      searching: false,
      searchData: '',
    };

    this.handleFolderClick = this.handleFolderClick.bind(this);
    this.handleCrumbClick = this.handleCrumbClick.bind(this);
    this.handleElevateClick = this.handleElevateClick.bind(this);
  }

  componentDidMount() {
    axios.get('scan')
      .then(response => this.setState({files: response.data}))
      .catch(error => console.log(error));
  }

  handleFolderClick(path) {
    this.setState({currentPath: path, searchData: '', searching: false});
  }

  handleCrumbClick(path) {
    this.setState({currentPath: path, searchData: '', searching: false});
  }

  handleElevateClick(path) {
    this.setState({currentPath: path, searchData: '', searching: false});
  }

  handleSearchInput(e) {
    let value = e.target.value;
    let search = value.length !== 0;

    this.setState({searching: search, searchData: value});
  }

  getItems(path, data, searchTerms, searching) {
    let pathArr = path.split('/').slice(1);
    let objPath = this.state.files.items;
    let searchItems = [];

    if (searching) {
      if (data.length) {
        data.forEach(d => {
          if (d.type === 'folder') {
            this.getItems('', d.items, searchTerms, true);

            if (d.name.toLowerCase().match(searchTerms)) {
              searchItems.push(d);
            }
          }
          else if (d.type === 'file') {
            if (d.name.toLowerCase().match(searchTerms)) {
              searchItems.push(d);
            }
          }
        });
      }
      return searchItems;
    }
    else if (!searching) {
      if (objPath.length) {
        pathArr.forEach(p => {
          let i = objPath.findIndex(o => o.name === p);
          objPath = objPath[i].items;
        });
        return objPath;
      }
    }
    return [];
  }

  render() {
    let currentItems = this.getItems(this.state.currentPath, this.state.files.items, this.state.searchData, this.state.searching);
    return (
      <div className="filemanager">
        <Searchbar value={this.state.searchData}
                   onChange={e => this.handleSearchInput(e)} />
        <Elevate path={this.state.currentPath}
                 onClick={path => this.handleElevateClick(path)} />
        <Breadcrumbs path={this.state.currentPath}
                     searching={this.state.searching}
                     onClick={path => this.handleCrumbClick(path)} />
        <Display items={currentItems}
                 searching={this.state.searching}
                 onClick={path => this.handleFolderClick(path)} />
      </div>
    );
  }
}

export default App;
