import React, {Component} from 'react';
import './Searchbar.css';

class Searchbar extends Component {
  render() {
    return (
      <div className="search">
        <input type="search"
               placeholder="Find a file.."
               value={this.props.value}
               onChange={this.props.onChange} />
      </div>
    );
  }
}

export default Searchbar;
