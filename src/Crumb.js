import React, {Component} from 'react';

class Crumb extends Component {
  render() {
    let style = this.props.last ? 'lastCrumb' : 'crumb';
    return (
      <div className={style} onClick={this.props.onClick}>
        {this.props.folder}
      </div>
    );
  }
}

export default Crumb;
