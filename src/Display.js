import React, {Component} from 'react';
import FolderPanel from './FolderPanel';
import FilePanel from './FilePanel';
import './Display.css';

class Display extends Component {
  sortItems(items) {
    let folders = [];
    let files = [];

    items.forEach(o => {
      if(o.type === "folder"){
        folders.push(o);
      }
      else if(o.type === "file"){
        files.push(o);
      }
    });

    return {folders: folders, files: files};
  }

  renderPanels(sortedItems){
    let panels = [];
    let i = 0;

    sortedItems.folders.forEach(d => {
      panels.push(<FolderPanel key={i++} folder={d} onClick={() => this.props.onClick(d.path)}/>);
    });

    sortedItems.files.forEach(f => {
      panels.push(<FilePanel key={i++} file={f} />);
    });

    return panels;
  }

  render() {
    let sortedItems = this.sortItems(this.props.items);
    let panels = this.renderPanels(sortedItems);

    if (panels.length === 0) {
      return (
        <div className="nothingfound" key={0}>
          <div className="nofiles"></div>
          <span>No files here.</span>
        </div>
      );
    }
    else {
      return (
        <ul className="data">
          {panels}
        </ul>
      );
    }
  }
}

export default Display;
