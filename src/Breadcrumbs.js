import React, {Component} from 'react';
import Crumb from './Crumb';
import './Breadcrumbs.css';

class Breadcrumbs extends Component {
  renderCrumbs() {
    let pathArr = this.props.path.split('/');
    let crumbs = []
    const slash = '/';
    let j = 0

    pathArr.forEach((d, i) => {
      let last = (i === (pathArr.length - 1));
      let path = pathArr.slice(0, (i+1)).join('/');

      crumbs.push(
        <Crumb key={j++}
               folder={d}
               last={last}
               onClick={() => this.props.onClick(path)}  />
             );

      if (!last) {
        crumbs.push(<div className="slash" key={j++}>{slash}</div>);
      }
    });

    return crumbs;
  }

  render() {
    let crumbs = [];
    if (this.props.searching){
      crumbs.push(<div className="lastcrumb" key={0}>Search results:</div>)
    }
    else {
      crumbs = this.renderCrumbs();
    }
    return (
      <div className="breadcrumbs">
        {crumbs}
      </div>
    );
  }
}

export default Breadcrumbs;
