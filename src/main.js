import React from 'react';
import ReactDOM from 'react-dom';
import '../favicons/favicons';
import App from './App';

document.addEventListener('DOMContentLoaded', function() {
  ReactDOM.render(<App />, document.getElementById('mount'));
});
