import React, {Component} from 'react';
import './Elevate.css';

class Elevate extends Component {
  render() {
    let pathArr = this.props.path.split('/');

    if (pathArr.length > 1) {
      let path = pathArr.slice(0, pathArr.length - 1).join('/');
      return <div className="elevate" onClick={() => this.props.onClick(path)}></div>
    }
    else {
      return <div className="top"></div>
    }
  }
}

export default Elevate;
