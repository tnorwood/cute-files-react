import React, {Component} from 'react';
import './FilePanel.css';

class FilePanel extends Component {
  convertBytesToReadable(bytes) {
    const sizes = ['Bytes', 'kB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return '0 Bytes';
    let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
  }

  parseFileType(name) {
    let fileType = name.split('.');
    fileType = fileType.length > 1 ? fileType[(fileType.length - 1)] : '';
    fileType = '.' + fileType.toLowerCase();

    let fileClass = 'icon file f-' + fileType;

    return [fileType, fileClass];
  }

  render() {
    let sizeString = this.convertBytesToReadable(this.props.file.size);
    let [fileType, fileClass] = this.parseFileType(this.props.file.name);

    return (
      <li>
        <a href={this.props.file.path} >
          <span className={fileClass}>{fileType}</span>
          <span className="name">{this.props.file.name}</span>
          <span className="details">{sizeString}</span>
        </a>
      </li>
    );
  }
}

export default FilePanel;
