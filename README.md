Cute-Files-React
---

Cute Files React is a node.js command line utility that turns your current
working directory into an online file browser.

Acknowledgements
---

Cute Files React is based on two of [tutorialzine's] (https://tutorialzine.com/) file browser tutorials,
written in [PHP] (https://tutorialzine.com/2014/09/cute-file-browser-jquery-ajax-php)
and [Node] (https://tutorialzine.com/2014/09/creating-your-first-node-js-command-line-application).
You can preview a live demo [here] (http://demo.tutorialzine.com/2014/09/cute-file-browser-jquery-ajax-php/).

This React version uses the CSS theme and file-scanning method from these examples,
but presents the front-end using React instead of JQuery.

Installation
---

This version is not available on npm so clone this repository, `cd` into the project directory and then globally install with:

```
npm link
```

Usage
---

To use, `cd` into the directory you want to share and run `cute-files-react`.

### Examples

To make the current working directory available on `<yourIP>:3000` on the local network:

```
cute-files-react
```

To make the current working directory available on `<yourIP>:1234` on the local network:

```
cute-files-react --port 1234
```
or
```
cute-files-react -p 1234
```

Notes
---

Files starting with a dot are not served.
