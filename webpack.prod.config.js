var baseConfig = require("./webpack.config.js");
var UglifyJSPlugin = require("uglifyjs-webpack-plugin");
var webpack = require("webpack");

baseConfig.plugins = [
  new UglifyJSPlugin()
];

module.exports = baseConfig;
