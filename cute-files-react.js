#!/usr/bin/env node

const express = require('express');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.js');
const ip = require('ip');
const os = require('os');
const networkInterfaces = os.networkInterfaces();
const path = require('path');
const contentDisposition = require('content-disposition');
const pkg = require( path.join(__dirname, 'package.json') );
const scan = require('./scan');
const program = require('commander');
const app = express();

const compiler = webpack(webpackConfig);

program
  .version(pkg.version)
  .option('-p, --port <port>', 'Port on which to listen to (defaults to 3000)', parseInt)
  .parse(process.argv);

let port = program.port || 3000;

let tree = scan('.', 'Shared');

app.use(express.static(__dirname + '/static'));

app.use('/Shared', express.static(process.cwd(), {
  index: false,
  setHeaders: function(res, path) {
    res.setHeader('Content-Disposition', contentDisposition(path))
  }
}));

app.get('/scan', function(req, res) {
  res.send(tree);
});

const server = app.listen(port, '0.0.0.0', function() {
  const host = server.address().address;
  const localip = ip.address();
  console.log('Cute Files is running at http://%s:%s', host, port);
  console.log('Network Interfaces:\n');

  for(var key in networkInterfaces){
    networkInterfaces[key].forEach(function(obj){
      if(obj.family === 'IPv4'){
        console.log('\t' + key + ' : ' + obj.address);
      }
    });
  }
  console.log('');
});
